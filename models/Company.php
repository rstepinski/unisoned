<?php
namespace app\models;

use yii\db\ActiveRecord;

class Company extends ActiveRecord
{
				const 
								STATUS_NEW = 0,
								STATUS_APPROVED = 1;
				
    public static function tableName()
    {
        return 'company';
    }
}