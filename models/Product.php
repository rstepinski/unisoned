<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\helpers\UrlFileReadHelper;
use app\helpers\DateTimeHelper;
use yii\data\ActiveDataProvider;
use app\helpers\CurrencyHelper;
use yii\helpers\ArrayHelper;
use Yii;

class Product extends ActiveRecord
{    
				const 
								STATUS_NEW = 0,
								STATUS_APPROVED = 1,
				
								ITEMS_PER_PAGE = 50;
				
				
    public static function tableName() 
    {
        return 'product';
    }
    
    public function fields() 
    {
        $fields = parent::fields();
        
        $fields[] = 'company';
        $fields[] = 'category';
        
        return $fields;
    }
				
				public function attributeLabels()	
				{
							 return [
												'name' => 'Name',
												'url' => 'Website',
												'price' => 'Price',
											 'priceElement' => 'Price CSS path',
												'priceCurrency' => 'Price currency',
								];
				}
    
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'companyId']);
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
				
				public function getAuthor()
				{
								return $this->hasOne(User::className(), ['id' => 'authorId']);
				}
				
				/**
				 * 
				 * @param array $item new product properties
				 */
				
				public static function create($item)
				{	
								$product = Product::findOne(['name' => $item['name'], 'status' => Product::STATUS_APPROVED]);
								
								if ($product === null)
								{
												$product = new Product;

												$product->name = $item['name'];
												$product->url = $item['url'];
												$product->priceElement = $item['priceElement'];

												$product->companyId = Company::findOne(['name' => $item['company']])->id;
												$product->categoryId = Category::findOne(['name' => $item['category']])->id;

												$product->authorId = Yii::$app->user->identity->id;
												$product->timeAdded = DateTimeHelper::now();
												$product->status = Product::STATUS_NEW;

												if ($product->save())
												{
																return true;
												}
								}
								else
								{
												Yii::$app->session->addFlash('danger', "Product $product->name already exists.");
								}
								return false;
				}
				
    public function getById($id)
    {
        return self::findOne(['id' => $id]);
    }
    
    public function getByCategory($category)
    {
        return self::find()
                ->joinWith(['company', 'category'])
                ->where(['categoryId' => $category])
                ->orderBy('company.name ASC, name ASC')
                ->all();
    }
    
    public function getByAuthor($author)
    {
        return self::find()->where(['authorId' => $author])->all();
    }
    
    public function getAll($page, $sort, $onlyApproved)
    {
								$offset = ($page - 1) * self::ITEMS_PER_PAGE;
								
								$condition = $onlyApproved ? ['status' => self::STATUS_APPROVED] : [];
								
								switch($sort)
								{
												case 'price':
																return self::find()->where($condition)->orderBy('priceCacheUSD')->limit(self::ITEMS_PER_PAGE)->offset($offset)->all();
												
												case 'producer':
																return self::find()->where($condition)->joinWith(['company'])->orderBy('company.name, name')->limit(self::ITEMS_PER_PAGE)->offset($offset)->all();
																
												case 'category':
																return self::find()->where($condition)->joinWith(['category'])->orderBy('category.name, name')->limit(self::ITEMS_PER_PAGE)->offset($offset)->all();
																
												default:
																return self::find()->where($condition)->orderBy('name')->limit(self::ITEMS_PER_PAGE)->offset($offset)->all();
								}
    }
    
    public function getPrice()
    {
								if ($this->priceNeedsRefresh)
								{
												return $this->updatePrice();
								}
								else
								{
										  return $this->priceCache;
								}
    }
				
				public function getPriceNeedsRefresh()
				{
								if (DateTimeHelper::compare(DateTimeHelper::add(1, 'days', $this->priceCacheTime), DateTimeHelper::now()) === 1)
								{
												return true;
								}
								if ($this->priceCache == 0)
								{
												return true;
								}
								return false;
				}
				
				public function getStarRating()
				{
								return round($this->getRating());
				}
				
				public function getRating()
				{
								$ratings = Rating::findAll(['productId' => $this->id]);
								
								if (!empty($ratings))
								{
												$count = 0;
												$ratingSum = 0;
												
												foreach ($ratings as $rating)
												{
																$count++;
																$ratingSum += $rating->rating;
												}
												
												return round($ratingSum / $count, 2);
								}
								else
								{
												return false;
								}
				}
				
				public function updatePrice()
				{
        $url = $this->url;
        $element = $this->priceElement;
								
								$doc = \PhpQuery::newDocumentHTML(UrlFileReadHelper::readFile($url));
								
								if ($doc == 'ERR_404') { return 'E_404'; }
								
								\PhpQuery::selectDocument($doc);

								$html = pq($element)->html();
								$matches = [];
								$price = 0;
								
								preg_match_all('#([\$£€ ])+([0-9,\.]+)#', $html, $matches);
								
								if (!isset($matches[0][0]))
								{
												$price = 'Error';
								}
								else
								{
												$price = ((count($matches[2]) > 1) ? $matches[2][1] : $matches[2][0]);
												$price = str_replace(',', '', $price);

												$curr = $matches[0][0];

												if (strpos($curr, '$') !== false) { $this->priceCurrency = 'USD'; }
												if (strpos($curr, '€') !== false) { $this->priceCurrency = 'EUR'; }
												if (strpos($curr, '£') !== false) { $this->priceCurrency = 'GBP'; }
								}
								
								$converter = new CurrencyHelper;
								
								$this->priceCache = $price;
								$this->priceCacheTime = DateTimeHelper::now();
								$this->priceCacheUSD = is_numeric($price) ? $this->priceCurrency == 'USD' ? $price : $converter->convert($price,	$this->priceCurrency,	'USD') : null;
								$this->save();

								return $price;
				}
				
				public function getStatusLabel()
				{
								if (isset(self::enumStatusLabels()[$this->status]))
								{
												return self::enumStatusLabels()[$this->status];
								}
								
								return null;
				}
				
				public function getStatusStyle()
				{
								if (isset(self::enumStatusStyles()[$this->status]))
								{
												return self::enumStatusStyles()[$this->status];
								}
								
								return null;
				}
				
				public function getStatusBadge()
				{
								return '<span class="label label-'.$this->getStatusStyle().'">'.$this->getStatusLabel().'</span>';
				}
				
				public static function enumStatusLabels()
				{
								return [
												self::STATUS_APPROVED => 'Approved',
												self::STATUS_NEW => 'New',
								];
				}
				
				public static function enumStatusStyles()
				{
								return [
												self::STATUS_APPROVED => 'success',
												self::STATUS_NEW => 'warning',
								];
				}
}

