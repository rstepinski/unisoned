<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use app\helpers\DateTimeHelper;

class Subscription extends ActiveRecord
{
				const ITEMS_PER_PAGE = 20;
				
				public static function tableName()	
				{
							 return 'subscription';
				}
				
				public static function getByUser($userId, $page = 1)
				{
								$offset = ($page - 1) * self::ITEMS_PER_PAGE;
								
								return self::find()->joinWith('product')->offset($offset)->limit(self::ITEMS_PER_PAGE)->where(['userId' => $userId])->all();
				}
				
				public function getProduct()
				{
								return $this->hasOne(Product::className(), ['id' => 'productId']);
				}
				
				/**
				 * 
				 * @param Product/integer $product - Product object or product id
				 * @param boolean $logFlashes - Determines whether to set flashes.
				 * @return integer/boolean true or false whether the update was succesful. Will also return false if object with the same user and product id already exists.
				 */
				public static function create($product, $logFlashes = true)
				{
								$user = Yii::$app->user->identity;

								$_product = ($product instanceof Product) ? $product : Product::findOne(['id' => $product, 'status' => Product::STATUS_APPROVED]);
												
								$subscription = Subscription::findOne(['productId' => $_product->id, 'userId' => $user->id]);

								if ($subscription === null)
								{
												$subscription = new Subscription;

												$subscription->userId = $user->id;
												$subscription->productId = $_product->id;
												$subscription->timeAdded = DateTimeHelper::now();

												if ($subscription->save())
												{
																if ($logFlashes) { Yii::$app->session->setFlash('success', "Your subscription for product $_product->name has been succesfully registered."); }
																return true;
												}
												else
												{
																if ($logFlashes) { Yii::$app->session->setFlash('danger', "There was an error while processing your request. Please try again in a few minutes. If the problem persists, please contact us."); }
																return false;
												}
								}
								else
								{
												if ($logFlashes) { Yii::$app->session->setFlash('danger', "You have already subscribed to this product."); }
												return false;
								}
				}
}