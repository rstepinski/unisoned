<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
				const
								STATUS_INACTIVE = 0,
								STATUS_ACTIVE = 1,
								STATUS_SUSPENDED = 2,
												
								ROLE_USER = 0,
								ROLE_ADMIN = 1;
				
				
				public static function tableName()	
				{
							 return 'user';
				}
				
				public function getSubscriptions()
				{
								return $this->hasMany(Subscription::className(), ['userId' => 'id']);
				}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return password_verify($password, $this->password);
    }
				
				public function sendActivationEmail($resend = false)
				{
								Yii::$app->mailer->compose('activation', ['user' => $this, 'resend' => $resend])
																								->setFrom(Yii::$app->params['adminEmail'])
																								->setTo($this->email)
																								->setSubject('UNISONED account activation')
																								->send();
				}
				
				public function hasSubscription($for)
				{
								$subscription = Subscription::find()->where(['userId' => $this->id, 'productId' => $for])->one();
								
								return $subscription != null;
				}
				
				
				public function getStatusLabel()
				{
								if (isset(self::enumStatusLabels()[$this->status]))
								{
												return self::enumStatusLabels()[$this->status];
								}
								
								return null;
				}
				
				public function getStatusStyle()
				{
								if (isset(self::enumStatusStyles()[$this->status]))
								{
												return self::enumStatusStyles()[$this->status];
								}
								
								return null;
				}
				
				public function getRoleLabel()
				{
								if (isset(self::enumRoleLabels()[$this->role]))
								{
												return self::enumRoleLabels()[$this->role];
								}
								
								return null;
				}
				
				public function getRoleStyle()
				{
								if (isset(self::enumRoleStyles()[$this->role]))
								{
												return self::enumRoleStyles()[$this->role];
								}
								
								return null;
				}
				
				public function getStatusBadge()
				{
								return '<span class="label label-'.$this->statusStyle.'">'.$this->statusLabel.'</span>';
				}
				
				public function getRoleBadge()
				{
								return '<span class="label label-'.$this->roleStyle.'">'.$this->roleLabel.'</span>';
				}
				
				public static function enumStatusLabels()
				{
								return [
												self::STATUS_INACTIVE => 'Inactive',
												self::STATUS_ACTIVE => 'Active',
												self::STATUS_SUSPENDED => 'Suspended',
								];
				}
				
				public static function enumStatusStyles()
				{
								return [
												self::STATUS_INACTIVE => 'default',
												self::STATUS_ACTIVE => 'success',
												self::STATUS_SUSPENDED => 'warning',
								];
				}
				
				public static function enumRoleLabels()
				{
								return [
												self::ROLE_USER => 'User',
												self::ROLE_ADMIN => 'Admin',
								];
				}
				
				public static function enumRoleStyles()
				{
								return [
												self::ROLE_USER => 'default',
												self::ROLE_ADMIN => 'danger',
								];
				}
}
