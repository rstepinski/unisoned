<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use app\helpers\DateTimeHelper;

class SignupForm extends Model
{
    public $username;
    public $password;
				public $passwordRepeat;
				public $email;
				public $currency;
				public $captcha;
				
				public function attributeLabels()
				{
								return [
												'passwordRepeat' => 'Repeat password',
								];
				}


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password', 'passwordRepeat', 'email', 'currency'], 'required', 'message' => 'This field cannot be blank.'],
												[['username'], 'string', 'max' => 40, 'min' => 3],
												['username', 'unique', 'targetClass' => 'app\models\User'],
												['email', 'unique', 'targetClass' => 'app\models\User'],
												[['password'], 'string', 'min' => 6],
												['passwordRepeat', 'compare', 'compareAttribute' => 'password'],   
												['email', 'email'],
												//['captcha', 'captcha'],
												['currency', 'string', 'length' => 3]
								];
    }
				
				public function register()
				{
								if ($this->validate())
								{
												$user = new User;
												
												$user->username = $this->username;
												$user->password = password_hash($this->password, PASSWORD_BCRYPT);
												$user->email = $this->email;
												$user->currency = $this->currency;
												
												$user->activationCode = Yii::$app->security->generateRandomString(10);
												$user->status = User::STATUS_INACTIVE;
												$user->timeCreated = DateTimeHelper::now();
											
												if($user->save())
												{
																$user->sendActivationEmail();
																
																return true;
												}
												return false;
								}
				}
}
