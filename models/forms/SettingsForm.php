<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * ContactForm is the model behind the contact form.
 */
class SettingsForm extends Model
{
    public $username;
    public $email;
    public $currency;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['username', 'email', 'currency'], 'required'],
												['username', 'uniqueExceptSelf'],
												['email', 'uniqueExceptSelf'],
												['username', 'string', 'max' => 30, 'min' => 3],
												['currency', 'string', 'length' => 3],
            // email has to be a valid email address
            ['email', 'email'],
        ];
				}
				
				public function uniqueExceptSelf($attribute, $params)
				{
								$user = User::findOne([$attribute => $this->$attribute]);
								
								if ($user !== null && $user->$attribute != Yii::$app->user->identity->$attribute) 
								{
												$this->addError($attribute, 'This '.$attribute.' has already been taken.');
								}
				}
				
				public function update()
				{
								if ($this->validate())
								{
												$user = Yii::$app->user->identity;

												$user->username = $this->username;
												$user->email = $this->email;
												$user->currency = $this->currency;

												return $user->save();
								}
				}
}
