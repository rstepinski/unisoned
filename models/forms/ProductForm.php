<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\Product;

/**
 * ContactForm is the model behind the contact form.
 */
class ProductForm extends Model
{
    public $name;
    public $url;
    public $priceElement;
    public $company;
    public $category;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'url', 'priceElement', 'company', 'category'], 'required'],
												['name', 'string', 'max' => 100, 'min' => 3],
												['url', 'url'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function update($id)
    {
								$product = Product::findOne(['id' => $id]);
								if ($product !== null)
								{
												$product->name = $this->name;
												$product->url = $this->url;
												$product->priceElement = $this->priceElement;
												$product->companyId = $this->company;
												$product->categoryId = $this->category;
								
												$product->save();
												return true;
								}
								return false;
    }
}
