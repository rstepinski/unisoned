<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PasswordChangeForm extends Model
{
    public 
								$password,
								$newPassword,
								$newPasswordRepeat;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['password', 'newPassword', 'newPasswordRepeat'], 'required'],
												[['newPassword', 'newPasswordRepeat'], 'string', 'max' => 40, 'min' => 6],
												['newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword'],
												['password', 'validatePassword'],
												['currency', 'string', 'length' => 3],
        ];
				}
				
				public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = Yii::$app->user->identity;

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, '');
            }
        }
    }
				
				public function attributeLabels()
				{
								return [
												'password' => 'Current password',
												'newPassword' => 'New password',
												'newPasswordRepeat' => 'Repeat new password',
								];
				}
				
				public function updatePassword()
				{
								$user = Yii::$app->user->identity;
								
								$user->password = password_hash($this->newPassword, PASSWORD_BCRYPT);
								
								return $user->save();
				}
}
