<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\forms\ContactForm */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        Thank you for contacting us. We well respond as soon as possible.
    </div>
				
    <?php else: ?>

    <div class="row">
				<p>
        If you have a question regarding the site functionalities or you've spotted a bug, please contact us.
								Note that you can add products to the database on your own, this option is accessible through your user panel (they will then have to be approved by the admin).
				</p>
				</div>

    <div class="row">
        <div class="col-lg-9" style="padding-left: 0;">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name') ?>
                <?= Yii::$app->user->isGuest ? $form->field($model, 'email') : $form->field($model, 'email')->textInput(['readonly' => true, 'value' => Yii::$app->user->identity->email]) ?>
                <?= $form->field($model, 'subject') ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                <?= Yii::$app->user->isGuest ? $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) : '' ?>
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php endif; ?>
</div>
