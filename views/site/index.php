<?php
/* @var $this yii\web\View */
$this->title = 'Unisoned';
use yii\helpers\Url;
?>
<div class="site-index">
				<div class="main">
								<div class="jumbotron">
												<h1 class="site-title" style="color: #fff">Unisoned</h1>
												<h2>The only DAW and sample library price comparison</h2>
												<br/>
												<div class="welcome">
																<div><i class="glyphicon glyphicon-ok"></i> Daily price updates!</div>
																<div><i class="glyphicon glyphicon-ok"></i> Price conversion!</div>
																<div><i class="glyphicon glyphicon-ok"></i> So far <?= Yii::$app->db->createCommand('SELECT COUNT(*) FROM product')->queryAll()[0]['COUNT(*)']; ?> products in database!</div>
																<div><i class="glyphicon glyphicon-ok"></i> Email notifications when price changes - never miss a sale again!</div>
												</div>
												<br/>
												<p><a class="btn btn-lg btn-success" href="<?= Url::toRoute(['product/list']); ?>">See the list</a></p>
								</div>
				</div>

				<div class="secondary">
    <div class="body-content" style="text-align: center;">
        <div class="row">
            <div class="col-lg-4">
                <h2>How it works</h2>
                <p>The server opens the product page and retrieves the price. It is then saved in the database and refreshed once a day. 
																Don't worry about your internet packet though - database update is completely server-side - what you get is only the retrieved prices.</p>
            </div>
												<div class="col-lg-4">
                <h2>Email subscription</h2>
                <p>It's both free of spam and free of charge! We will never send you unwanted messages, only notifications about price changes
																for the products you selected. The only requirement is you have to be a registered user. Users can also rate the libraries.</p>
																<p>
            </div>
												<div class="col-lg-4">
                <h2>Price conversion</h2>
                <p>The system automatically converts prices to one currency to make them easier to compare. It defaults to the US Dollar, but
																if you are a registered user you can select your own currency.</p>
            </div>
        </div>
							<a class="btn btn-lg btn-primary" href="<?= Url::toRoute(['site/signup']) ?>">Sign up now!</a>
    </div>
				</div>
</div>
