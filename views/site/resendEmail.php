<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\helpers\AlertHelper;

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-resendEmail">

				<?= AlertHelper::get() ?>
								
								<?php $form = ActiveForm::begin([
												'id' => 'signup-form',
												'options' => ['class' => 'form-horizontal'],
												'fieldConfig' => [
																'template' => "<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
												],
												'action' => Url::to(['site/resend-email']),
								]); ?>
								
								<p>Please enter your username or email</p>
												<?= $form->field($model, 'email_username') ?>
												<?= Html::submitButton('Resend', ['class' => 'btn btn-primary', 'name' => 'resend-button']) ?>
								
								<?php ActiveForm::end(); ?>
</div>
