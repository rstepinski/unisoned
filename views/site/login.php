<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\helpers\AlertHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
				
				<?= AlertHelper::get() ?>
				
    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-xs-5\">{input}</div>\n<div class=\"col-xs-5\">{error}</div>",
            'labelOptions' => ['class' => 'col-xs-2 control-label'],
        ],
    ]); ?>
				
    <?= $form->field($model, 'username') ?>
				
    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="col-xs-offset-2"><?= $form->field($model, 'rememberMe')->checkbox() ?></div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="col-xs-offset-2">
        Don't have an account? <a href="<?= Url::to(['site/signup']) ?>">Click here to sign up.</a><br>
        Didn't receive the activation email? <a href="<?= Url::to(['site/resendEmail']) ?>">Click here to request another one.</a>
    </div>
</div>
