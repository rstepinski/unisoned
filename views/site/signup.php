<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use app\helpers\CurrencyHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\forms\SignupForm */

$this->params['breadcrumbs'][] = $this->title;

$currencies = [];

foreach (CurrencyHelper::AVAILABLE_CURRENCIES as $iso => $currency)
{
				$c = $iso.' - '.$currency;
				$currencies[$iso] = $c;
}

												
?>
<div class="site-signup">
				
				<?php if ($success): ?>
				
				<div class="info bg-success col-lg-12">You've been succesfully registered. Please check your mailbox for activation email. If you didn't receive one, please <a href="<?= Url::to(['site/resend-email'])?>">click here to resend the email.</a></div>
				
				<?php else: ?>

								<p>Please fill out all fields to sign up:</p>

								<?php $form = ActiveForm::begin([
												'id' => 'signup-form',
												'options' => ['class' => 'form-horizontal'],
												'fieldConfig' => [
																'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
																'labelOptions' => ['class' => 'col-lg-2 control-label'],
												],
								]); ?>

								<?= $form->field($model, 'username') ?>
								<?= $form->field($model, 'email') ?>
								<?= $form->field($model, 'password')->passwordInput() ?>
								<?= $form->field($model, 'passwordRepeat')->passwordInput() ?>
								<?= $form->field($model, 'currency')->dropDownList($currencies); ?>
								<?= $form->field($model, 'captcha')->widget(Captcha::className(), [
												'template' => '<div class="row"><div class="col-lg-5">{image}</div><div class="col-lg-7">{input}</div></div>',
								]) ?>

								<div class="form-group">
												<div class="col-lg-offset-2 col-lg-10">
																<?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
												</div>
								</div>

							 <?php ActiveForm::end(); ?>

				<?php endif; ?>
</div>
