<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="row">
								<div class="col-xs-2">
												<div class="pull-right" style="margin-left: 12px; position: relative; left: 20px"><img class="img-circle" width="140" height="140" src="<?= Url::to('@web/img/me.png') ?>"/></div>
								</div>
								<div class="col-xs-10" style="text-align: justify; vertical-align: middle; margin-top: 3px;">
												<blockquote>
																<p>
																I'm a pianist, a guitarist, a composer and a programmer. I have great interest in film music and I know that these days, quality of samples is extremely important.
																Sometimes even more than the music itself (which is a shame).
																But with that many sample libraries out there, how do you choose the right one of good quality and not-so-big price? You can use a price comparator.
																Oh, wait, but there isn't one dedicated to music software, is there? <b>Now, there is.</b> Discover many new libraries you had no idea about (neither had I) and
																see all their prices in one place.
																</p>
																<footer>Radosław Stępiński, the creator</footer>
												</blockquote>
								</div>
				</div>
</div>
