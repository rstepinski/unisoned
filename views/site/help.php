<?php

use yii\helpers\Html;
use app\helpers\CurrencyHelper;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

?>
<div class="site-help">
				<?php $i = 0 ?>
				
				<div class="col-lg-4">
    <?php foreach(CurrencyHelper::AVAILABLE_CURRENCIES as $iso => $currency)
				{ $i++; ?>
				<div class="row <?= $i%2==0?'even':'odd'?>">
								<div class="col-xs-3"><?= $iso ?></div>
								<div class="col-xs-9"><?= $currency.(CurrencyHelper::getSymbol($iso) != $iso ? ' ('.CurrencyHelper::getSymbol($iso).')' : '')?></div>
				</div>
				<?php } ?>
</div>
								
</div>
