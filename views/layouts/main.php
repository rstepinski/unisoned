<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\User;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
				<link rel="icon" type="ico" href="<?= Url::to('@web/favicon.ico') ?>" />
    <title><?= Html::encode($this->title) ?></title>
				<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
				<link rel="stylesheet" href="http://css-spinners.com/css/spinner/three-quarters.css" type="text/css">
				<link href="<?= Url::to('@web/css/font.css') ?>" rel="stylesheet" />
				<script type="text/javascript" src="<?= Url::to('@web/js/rate.js')?>"></script>
				<script type="text/javascript" src="<?= Url::to('@web/js/widths.js')?>"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/bootstrap.css') ?>">
				<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/font-awesome.css') ?>">
				<script type="text/javascript">
				/* To initialize BS3 tooltips set this below */
				$(function () { 
								$("[data-toggle='tooltip']").tooltip(); 
				});;
				/* To initialize BS3 popovers set this below */
				$(function () { 
								$("[data-toggle='popover']").popover(); 
				});
				$(function () {
								$(".dropdown-toggle").dropdown();
				})
				</script>
    <?php $this->head() ?>
			 
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap <?= '_'.Yii::$app->controller->id ?> <?= '_'.Yii::$app->controller->action->id ?>">
        <?php
            NavBar::begin([
                'brandLabel' => 'Unisoned',//Html::img(Url::to('@web/img/unisoned_b_1.png'), ['height' => 70]),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
																'encodeLabels' => false,
                'items' => [
                    ['label' => '<i class="fa fa-home fa-fw"></i> Home' , 'url' => ['/site/index']],
																				['label' => 'Product list', 'url' => ['/product/list']],
                    ['label' => 'About', 'url' => ['/site/about']],
                    ['label' => 'Contact', 'url' => ['/site/contact']],
                    Yii::$app->user->isGuest ?
                        ['label' => '<i class="fa fa-sign-in fa-fw"></i> Login', 'url' => ['/site/login']] :
                        [
																												'label' => '<i class="fa fa-user"></i> '.Yii::$app->user->identity->username,
																												'items' => [
																																['label' => '<i class="fa fa-envelope fa-fw"></i> Email subscriptions <span class="badge">'.Yii::$app->db->createCommand('SELECT COUNT(*) FROM subscription WHERE userId = '.Yii::$app->user->identity->id)->queryAll()[0]['COUNT(*)'].'</span>', 'url' => ['/subscription/list']],
																																['label' => '<i class="fa fa-plus-circle fa-fw"></i> Add product', 'url' => ['/product/create']],
																																'<li class="divider"></li>',
																																['label' => '<i class="fa fa-gear fa-fw"></i> Settings', 'url' => ['/user/update']],
																																['label' => '<i class="fa fa-gears fa-fw"></i> Admin panel', 'url' => ['/admin/list-products', 'p' => 0], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN],
																																'<li class="divider"></li>',
																																['label' => '<i class="fa fa-sign-out fa-fw"></i> Logout', 'url' => ['/user/logout']],
																												],
																								],
                ],
            ]);
            NavBar::end();
        ?>
								
        <div class="container">
												<?php if  (Yii::$app->controller->id == 'admin'): ?>
												<div class="sidebar-container col-lg-3 col-sm-5">
																<div class="sidebar">
																				<div class="header">
																								Admin panel
																				</div>
																				<?php echo Nav::widget([
																								'options' => ['class' => 'nav nav-sidebar'],
																								'encodeLabels' => false,
																								'items' => [
																												['label' => 'Product list' , 'url' => ['/admin/list-products']],
																												['label' => 'User list', 'url' => ['/admin/list-users']],
																												['label' => 'Company list', 'url' => ['/admin/list-companies']],
																												['label' => 'Category list', 'url' => ['/admin/list-categories']],
																												['label' => 'Server log', 'url' => ['/admin/console-log']],
																								],
																				]); ?>
																</div>
																<?php if (Yii::$app->controller->action->id == 'list-products'): ?>
																<div class="user-info">
																				<div class="header">
																								User info
																								<i class="working pull-right fa fa-gear fa-spin fa-2x" style="display: none; position: relative; bottom: 4px;"></i>
																				</div>
																				<div class="content">
																								<i>No user selected</i>
																				</div>
																</div>
																<?php endif; ?>
												</div>
												<?php endif; ?>
												
												<div class="content-wrapper <?= Yii::$app->controller->id == 'admin' ? 'col-lg-9 col-sm-7' : '' ?>">
															 <?= siteHeader(); ?>
																<div class="content"><?= $content ?></div>
												</div>
												<!-- <div class="ads-bottom row">
																<div class="col-xs-4"><img src="<?= Url::to('@web/img/ad_empty.png')?>"/></div>
																<div class="col-xs-4"><img src="<?= Url::to('@web/img/ad_empty.png')?>"/></div>
																<div class="col-xs-4"><img src="<?= Url::to('@web/img/ad_empty.png')?>"/></div>
												</div> -->
        </div>
    </div>
				
    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Radosław Stępiński <?= date('Y') ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php 
function siteHeader()
{
				$route = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
				
				$headers = [
								'site/about' => 'About',
								'site/help' => 'Supported currencies',
								'site/login' => 'Login',
								'site/contact' => 'Contact',
								'site/error' => 'Error',
								'site/resendEmail' => 'Resend activation email',
								'site/signup' => 'Sign up',
								
								'product/list' => 'Product list',
								'product/create' => 'Add products',
								'product/help' => 'Help',
								'product/update' => 'Update product',
								
								'admin/list-products' => 'Product list',
								'admin/console-log' => 'Console log',
								
								'subscription/list' => 'Your subscriptions',
								
								'user/update' => 'Account',
				];
				
				return isset($headers[$route]) ? '<div class="header"><h1 class="no-margin">'.$headers[$route].'</h1></div>' : null;
}
?>
