<?php

use app\helpers\AlertHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\helpers\PaginationHelper;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Subscription;


echo AlertHelper::get();

echo GridView::widget([
				'id' => 'list-subscriptions',
				'summary' => '',
				'dataProvider' => new ActiveDataProvider([
								'query' => Subscription::find()->where(['userId' => Yii::$app->user->identity->id]),
								'pagination' => [
												'pageSize' => 50,
								],
				]),
				'columns' => [
								'name' => [
												'label' => 'Name',
												'format' => 'html',
												'value' => function($data){
																return Html::a($data->product['name'], $data->product['url']);
												}
								],
								'timeAdded:datetime:Added',
								'remove' => [
												'label' => 'Remove',
												'format' => 'raw',
											 'value' => function($data){
																return '<a href="'.Url::to(['subscription/remove', 'product' => $data->product['id']]).'"><i class="glyphicon glyphicon-remove"></i></a>';
												},
												'contentOptions' => ['class' => 'text-right remove'],
								]
				]
]); ?>

<div class="subscription-add bg-info alert col-md-5">
				<div>
								<p>To subscribe to all products from a producer, select the company below and press <b>Add all.</b></p>
				</div>
				<form method="get" action="<?= Url::to(['subscription/add-by-company']) ?>">
				<div class="form">
								<div class="col-xs-10">
								<select class="form-control" name="company">
												<?php foreach ($companies as $company): ?>
																<option value="<?= $company->id ?>"><?= $company->name ?></option>
												<?php endforeach; ?>
								</select>
								</div>
								<div class="col-xs-2">
												<input type="submit" value="Add all" class="btn btn-danger btn-md" />
								</div>
				</div>
				</form>
</div>
