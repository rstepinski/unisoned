<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\helpers\AlertHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->params['breadcrumbs'][] = $this->title;

$tooltip = "Enter the CSS path of product's price. If you don't know what CSS path is, click the question mark to see how to retrieve it.";
$tooltip2 = "Unapproved companies are marked with (!)";

$maxRows = 15;

?>

<div class="product-create">
				
				<?= AlertHelper::get(); ?>
				
    <p>Enter the product(s) details and press Add.</p>
				
				<div class="info alert-maxReached bg-warning" onclick="dismissAlert()" style="display: none;">
								You can't add more than <?= $maxRows ?> entries at once.
				</div>
				
				<div class="alert alert-info">
								Before you add any product, go to products list and disable "only approved".
								Don't duplicate products that do not display an error instead of price.
				</div>

    <?php $form = ActiveForm::begin([
        'id' => 'create-form',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>

				<div class="product-form row">
								<div class="row header">
												<div class="name">Name</div>
												<div class="url">URL</div>
												<div class="priceElement">CSS Path <span data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?= $tooltip ?>"><a href="<?= Url::to(['product/help']) ?>"><i class="glyphicon glyphicon-question-sign"></i></a></span></div>
												<div class="company">Producer  <span data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?= $tooltip2 ?>"><i class="glyphicon glyphicon-question-sign"></i></span></div>
												<div class="category">Category</div>
												<div class="button"><i class="glyphicon glyphicon-plus" onclick="addRow()"></i></div>
							 </div>
				</div>

    <div class="row" style="margin-top: 5px; margin-left: -15px;">
        <div class="col-lg-11">
            <?= Html::submitButton('Add', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
				
				<div class="subscription-add alert-info alert col-lg-4">
								<div>
												<p>The producer is not on the list? Add it!</p>
								</div>
								<form method="get" action="<?= Url::to(['company/create']) ?>">
								<div class="form">
												<div class="col-lg-10">
																<input type="text" name="name" class="form-control" />
												</div>
												<div class="col-lg-2">
																<input type="submit" value="Add" class="btn btn-danger btn-md" />
												</div>
								</div>
								</form>
				</div>
</div>

<script type="text/javascript">

var i = 0;
var rowCount = 0;

var maxRows = <?= $maxRows ?>;

function generateField(attribute, type)
{
				var id = 'product-' + i + '-' + attribute;
				var name = 'Product[' + i + '][' + attribute + ']';
				
				if (type === 'select')
				{
								var options = "";
								
								var optionsJSON = (attribute === 'company') ? <?= $companies ?> : <?= $categories ?>;
							 
								for (var a = 0; a < optionsJSON.length; a++)
								{
												options += '<option>' + optionsJSON[a] + '</option>';
								}
								
								return '<select id="' + id + '" class="form-control" name="' + name + '">' + options + '</select>';
				}
				else
				{
								return '<input type="' + type + '" id="' + id + '" class="form-control" name="' + name + '" />';
				}
}

function addRow()
{
				if (rowCount + 1 < 20)
				{
								$('.product-form').append(
												'<div class="row" id="row-' + i + '">'
															+'<div class="name">' + generateField('name', 'text') + '</div>'
															+'<div class="url">' + generateField('url', 'text') + '</div>'
															+'<div class="priceElement">' + generateField('priceElement', 'text') + '</div>'
															+'<div class="company">' + generateField('company', 'select') + '</div>'
															+'<div class="category">' + generateField('category', 'select') + '</div>'
															+ ((i !== 0) ? '<div class="button"><i class="glyphicon glyphicon-remove" onclick="removeRow(' + i + ')"></i></div>' : '')
											+'</div>'
								);

								i++;
								rowCount++;
				}
				else
				{
								$('.alert-maxReached').show();
				}
}

function removeRow(row)
{
				$('#row-' + row).remove();
				rowCount--;
}				

function dismissAlert()
{
				$('.alert-maxReached').hide();
}

$(document).ready(addRow());
</script>