<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\helpers\StarRatingHelper;
use app\helpers\CurrencyHelper;
use yii\helpers\Url;
use app\helpers\AlertHelper;

echo AlertHelper::get();

?> 
<div class="row">
				<div style="font-size: 20px" class="col-md-6"> 
								<?php
								if ($onlyApproved) {echo Html::a('Only approved <i class="fa fa-toggle-on fa-lg" style="color: #070;"></i>', Url::to(['product/list', 'onlyApproved' => 0]), ['class' => 'toggle-approved']);}
								else	{echo Html::a('Only approved <i class="fa fa-toggle-off fa-lg" style="color: #900;"></i>', Url::to(['product/list', 'onlyApproved' => 1]), ['class' => 'toggle-approved']);}
								?> 
				</div>
				<div class="col-md-6">
								<form name="search" class="form-horizontal" method="get" action="<?= Url::to(['product/list']) ?>">
												<div class="input-group form-group">
																								<input class="form-control" id="search" name="search" type="text" placeholder="Search..." <?= isset($search) && $search !== null ? 'value="'.$search.'"' : '' ?>/>
																								<span class="input-group-addon submit" style="border-radius: 0;" onclick="function(){submit();}"><i class="fa fa-search fa-fw"></i></span>
												</div>
								</form>				
				</div>
</div>
<?php

echo GridView::widget([
	'id' => 'product-list',
	'summary' => '',
    'dataProvider' => $provider,
    'columns' => array_merge([
        'name' => [
			'format' => 'html',
			'value' => function($data){
				return	Html::a($data->name, $data->url);
			},
			'label' => 'Name',
			'attribute' => 'name',
			'contentOptions' => ['class' => 'name'],
		],
		'rating' => [
			'format' => 'raw',
			'value' => function($data){
				return StarRatingHelper::getStars($data->rating, $data->id, !Yii::$app->user->isGuest);
			},
			'label' => 'Rating',
			'contentOptions' => ['class' => 'text-center nowrap'],
			'attribute' => 'rating',
		],
        'company.name:text:Producer',
        'category.name:text:Category',
        'price' => [
			'format' => 'html',
            'value' => function($data){
                return '<span class="price">'.
					(is_numeric($data->price) && $data->price != 0 ? Yii::$app->formatter->asCurrency($data->price, CurrencyHelper::getSymbol($data->priceCurrency))
					: '<span class="label label-danger">Error 404</span>')
					.'</span>';
            },
            'label' => 'Price',
			'contentOptions' => ['class' => 'text-right nowrap'],
        ],
		'unifiedPrice' => [
			'format' => 'raw',
			'value' => function($data){
				$userCurrency = Yii::$app->user->isGuest ? 'USD' : Yii::$app->user->identity->currency;
																
				$dataAttributes = $userCurrency == $data->priceCurrency 
					? 'data-noupdate="true"'
					: 'data-value="'.$data->price.'" data-from="'.$data->priceCurrency.'" data-to="'.$userCurrency.'"';
													   
				$content = $userCurrency == $data->priceCurrency
					?  Yii::$app->formatter->asCurrency($data->price, CurrencyHelper::getSymbol($data->priceCurrency))
					: '<i class="fa fa-gear fa-spin"></i>';
																
				return  '<span class="price-unified" '.$dataAttributes.'>'.$content.'</span>';
			},
			'label' => 'Unified price',
			'attribute' => 'unifiedPrice',
			'sortLinkOptions' => ['class' => 'sort-numerical'],
			'contentOptions' => ['class' => 'text-right nowrap'],
		], 
    ],										
	Yii::$app->user->isGuest ? ([] : [
		'subscription' => [
			'value' => function($data){
				return Yii::$app->user->identity->hasSubscription($data->id)
					? Html::a('', Url::to(['subscription/remove', 'product' => $data->id]))
					: Html::a('', Url::to(['subscription/add', 'product' => $data->id]));
				},
				'format' => 'raw',
				'contentOptions' => function($data){
					return ['class' => Yii::$app->user->identity->hasSubscription($data->id)?'has-subscription':'no-subscription'];
				}
			]
		]),
]);
												

?>

<script type="text/javascript">
$(document).ready(function(){
			 $('#product-list table tbody tr').each(function(){
								if (!$(this).find(".price-unified").data('noupdate'))
								{			
												var value = $(this).find(".price-unified").data('value');
												var to = $(this).find(".price-unified").data('to');
												var from = $(this).find(".price-unified").data('from');
												
												var id = $(this).data('key');
												
												$.ajax({
																url: '<?= Url::to(['product/ajax-convert']) ?>' + '?value='+value+'&from='+from+'&to='+to,
																method: 'get',
												}).done(function(data){
															 update(id, data);
												});
								}
			 });
				
				$('.submit').click(function(){
								document.forms['search'].submit();
				});
});

function update(id, price)
{
				$(document).find('[data-key='+id+'] .price-unified').text(price);
}				
</script>
