<?php
use yii\helpers\Html;
use app\models\Product;
use yii\helpers\Url;
use app\helpers\CurrencyHelper;
use app\helpers\PaginationHelper;
use app\helpers\StarRatingHelper;
use app\helpers\AlertHelper;

/* @var $this yii\web\View */
$this->title = 'Product list';
$this->params['breadcrumbs'][] = $this->title;
$tooltip = 'The vaule is estimated using fixer.io API. It might and probably will slightly differ from real value.';
$tooltipError = 'Please check if you typed in your currency correctly. If it is correct, the system does not support this currency. Click the label to see which currencies are supported.';
$tooltipSubscription = 'Email subscription';
?>

<?= AlertHelper::get() ?>

<?php $currency = Yii::$app->user->isGuest ? 'USD' : Yii::$app->user->identity->currency; 
				  $converter = new CurrencyHelper;
?>

<div class="product-list">
    <div class="product row header">
        <div class="name col-md-4 <?= Yii::$app->user->isGuest ? '' : 'as-user' ?>">
                                        <div class="text-center"><?= $sort == 'name' ? '<i class="glyphicon glyphicon-sort-by-alphabet pull-left"></i>' : null ?><?= Html::a('Name', Url::to(['product/list', 'sort' => 'name'])) ?></div>
        </div>
        <?= !Yii::$app->user->isGuest ? '<div class="subscription pull-right"><span data-toggle="tooltip" title="'.$tooltipSubscription.'"><i class="glyphicon glyphicon-eye-open"></i></span></div>' : null ?>
        <div class="priceLocal pull-right"><div class="text-center">Unified price <span data-toggle="tooltip" title="<?= $tooltip ?>"><i class="glyphicon glyphicon-question-sign align-right"></i></span></div></div>
        <div class="price pull-right">
            <div class="text-center"><?= $sort == 'price' ? '<i class="glyphicon glyphicon-sort-by-attributes pull-right"></i>' : null ?><?= Html::a('Price', Url::to(['product/list', 'sort' => 'price'])) ?></div>
        </div>
        <div class="category pull-right">
            <div class="text-center"><?= $sort == 'category' ? '<i class="glyphicon glyphicon-sort-by-alphabet pull-left"></i>' : null ?><?= Html::a('Category', Url::to(['product/list', 'sort' => 'category'])) ?></div>
        </div>
        <div class="company pull-right">
            <div class="text-center"><?= $sort == 'producer' ? '<i class="glyphicon glyphicon-sort-by-alphabet pull-left"></i>' : null ?><?= Html::a('Producer', Url::to(['product/list', 'sort' => 'producer'])) ?></div>
        </div>
        <div class="rating col-md-1 pull-right"><div class="text-center">Rating</div></div>
    </div>
								
<?php

$i = 0;
foreach ($products as $product)
{ 
$i++; ?>
<div class="product row <?= $i%2==0 ? 'even' : 'odd' ?>" id="<?= $product->id; ?>">
    <div class="name col-md-4 <?= Yii::$app->user->isGuest ? '' : 'as-user' ?>"><a class="clear" href="<?= $product->url ?>"><?= $product->name; ?></a></div>
												
        <?php if (!Yii::$app->user->isGuest): ?>

            <div id="<?= $product->id ?>" class="subscription pull-right subscription-button <?= Yii::$app->user->identity->hasSubscription($product->id) ? 'has-subscription' : 'no-subscription' ?>">
                <?php if(Yii::$app->user->identity->hasSubscription($product->id)): ?>
                    <a href="<?= Url::to(['subscription/remove', 'product' => $product->id]) ?>"><i class="glyphicon glyphicon-eye-open"></i></a>
                <?php else: ?>
                    <a href="<?= Url::to(['subscription/add', 'product' => $product->id]) ?>"><i class="glyphicon glyphicon-eye-open" style="visibility: hidden;"></i></a>
                <?php endif; ?>
            </div> 

        <?php endif; ?>
												
        <div class="priceLocal align-right pull-right">
        <?php 
            $price = $product->price;
            $convertedPrice = $product->priceCurrency == $currency ? $price : $converter->convert($product->price, $product->priceCurrency, $currency);

            if (!is_numeric($convertedPrice)) { if ($i == 1) { ?> <a href="<?= Url::to(['site/help'])?>"><span class="label label-danger" data-toggle="tooltip" title="<?= $tooltipError ?>">Error</span></a> <?php }}
            else 
            {
                echo Yii::$app->formatter->asCurrency($convertedPrice, $converter->getSymbol($currency));
            } ?>
        </div>
        <div class="price align-right pull-right">
            <?= is_numeric($price) ? Yii::$app->formatter->asCurrency($price, $converter->getSymbol($product->priceCurrency)) : '<span class="label label-danger">'.$price.'</span>'; ?>
        </div>
        <div class="category pull-right"><?= $product->category['name'] ?></div>
        <div class="company pull-right"><?= $product->company['name'] ?></div>
        <div class="rating pull-right"><?= $product->starRating ? StarRatingHelper::getStars($product->rating, $product->id, !Yii::$app->user->isGuest) : (Yii::$app->user->isGuest ? '-' : StarRatingHelper::getStars(0, $product->id)) ?></div>				
</div>
    <?php } ?>
								
    <div class="page-nav"><?= $pages == 1 ? null : PaginationHelper::getButtons($pages, $page) ?></div>

    <?php if (!Yii::$app->user->isGuest && $onlyApproved): ?>
        <a class="btn btn-md btn-danger" href="<?= Url::to(['product/list', 'onlyApproved' => 0])?>">Show also unapproved products</a>  (this may cause big loading times)
    <?php elseif (!Yii::$app->user->isGuest && !$onlyApproved): ?>
        <a class="btn btn-md btn-success" href="<?= Url::to(['product/list', 'onlyApproved' => 1])?>">Show only approved products</a>
    <?php endif; ?>
</div>

<script type="text/javascript">

$(document).ready(function()
{
    $('.has-subscription').hover(
        function()
        { 
            $(this).children('a').css('color', '#ff3000');
            $(this).children('a').children('i').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
        }, 
        function()
        {
            $(this).children('a').css('color', '#444');
            $(this).children('a').children('i').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
        }
    );

    $('.no-subscription').hover(
        function()
        {
            $(this).children('a').children('i').css('visibility', 'visible');
        },
        function ()
        {
            $(this).children('a').children('i').css('visibility', 'hidden');
        }
    );
});
</script>