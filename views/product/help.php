<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = 'Help';
$this->params['breadcrumbs'][] = $this->title;
?>

<script type="text/javascript">

$('#chrome a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

$('#firefox a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

$('#iexplorer a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

$('#opera a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

</script>

<div class="product-help">
				<h1>Help</h1>

    <ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#chrome-opera" aria-controls="chrome-opera" role="tab" data-toggle="tab">Chrome / Opera</a></li>
								<li role="presentation"><a href="#firefox" aria-controls="firefox" role="tab" data-toggle="tab">Firefox</a></li>
								<li role="presentation"><a href="#iexplorer" aria-controls="iexplorer" role="tab" data-toggle="tab">Internet Explorer</a></li>
				</ul>

				<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="chrome-opera">
												<h3>Step 1: Determining the URL</h3>
												<p>Go to the producer's website and choose the product you want to add. Then, copy the full URL and paste it in the URL field.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/chrome/url.png'); ?>" />
												
												<h3>Step 2: Opening the Inspector</h3>
												<p>Right click the price and choose <b>"Inspect Element"</b>.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/chrome/inspect_element.jpg'); ?>" />
												
												<h3>Step 3: Determining the CSS path</h3>
												<p>Right click the price in the inspector window and choose <b>"Copy CSS Path"</b>. 
																				Make sure there is nothing else but price in the area (check what the inspector is highlighting), or the system might not and most probably 
																				will not be able to retrieve the price. Then paste the path in the CSS Path field.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/chrome/css_path.png'); ?>" />
												<p>
							 </div>
								<div role="tabpanel" class="tab-pane" id="firefox">
												<h3>Step 1: Determining the URL</h3>
												<p>Go to the producer's website and choose the product you want to add. Then, copy the full URL and paste it in the URL field.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/firefox/url.png'); ?>" />
												
												<h3>Step 2: Opening the Inspector</h3>
												<p>Right click the price and choose <b>"Inspect Element"</b>.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/firefox/inspect_element.jpg'); ?>" />
												
												<h3>Step 3: Determining the CSS path</h3>
												<p>Right click the price in the inspector window and choose <b>"Copy Unique Selector"</b>. 
																				Make sure there is nothing else but price in the area (check what the inspector is highlighting), or the system might not and most probably 
																				will not be able to retrieve the price. Then paste the path in the CSS Path field.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/firefox/css_path.png'); ?>" />
												<p>
								</div>
								<div role="tabpanel" class="tab-pane" id="iexplorer">
												<h3>Step 1: Determining the URL</h3>
												<p>Go to the producer's website and choose the product you want to add. Then, copy the full URL and paste it in the URL field.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/iexplorer/url.png'); ?>" />
												
												<h3>Step 2: Opening the Inspector</h3>
												<p>Right click the price and choose something along the lines of <b>"Inspect Element"</b> or <b>"Check Element"</b>. Unfortunately I wasn't able to install English version
												of IE on my computer, so the screenshot is in Polish.</p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/iexplorer/inspect_element.png'); ?>" />
												
												<h3>Step 3: Determining the CSS path</h3>
												<p>Unfortunately, IE doesn't allow you to copy the path so you have to manually type it in the field. Best practice is to use last two selectors and separate them
																				with space. For this example, the CSS path would be: <code>div.intro-inner p.excerpt</code></p>
												<img style="width: 100%" src="<?= Url::to('@web/img/help/iexplorer/css_path.jpg'); ?>" />
												<p></div>
				</div>
</div>
