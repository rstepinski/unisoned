<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\AlertHelper;
use app\models\Category;
use app\models\Company;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = 'Help';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="product-update">
				<?= AlertHelper::get() ?>
				
				<?php $form = ActiveForm::begin(['id' => 'update-form']); ?>
				<div clas="row">
								<div class="col-sm-4"><?= $form->field($model, 'name')->textInput(['value' => $product->name]) ?></div>
							 <div class="col-sm-4"><?= $form->field($model, 'url')->textInput(['value' => $product->url]) ?></div>
								<div class="col-sm-4"><?= $form->field($model, 'priceElement')->textInput(['value' => $product->priceElement]) ?></div>
				</div>
				
								<?php $model->company = $product->company['id']; ?>
								<?php $model->category = $product->category['id']; ?>
								
								<?php 
												$_cat = Category::find()->all();
												$_com = Company::find()->orderBy('status DESC, name ASC')->all();
												
												$categories = [];
												$companies = [];
												
												foreach ($_cat as $category) { $categories[$category->id] = $category->name; }
												foreach ($_com as $company) { $companies[$company->id] = $company->name; }
								?>
					
				<div class="row">
								<div class="col-sm-4"><?= $form->field($model, 'company')->dropDownList($companies); ?></div>
								<div class="col-sm-4"><?= $form->field($model, 'category')->dropDownList($categories); ?></div>
								<div class="col-sm-4" style="padding-top: 25px">
												<?= Html::submitButton('Update', ['class' => 'btn btn-primary col-sm-12', 'name' => 'contact-button']) ?>
								</div>
				</div>
				<?php ActiveForm::end(); ?>
</div>
