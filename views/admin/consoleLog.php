<?php

$patterns = [
				'#(\[IMPORTANT\].*?)<br/>#i' => '<span class="log-important">$1</span><br/>',
				'#(\[WARNING\].*?)<br/>#i' => '<span class="log-warning">$1</span><br/>',
				'#(\[ERROR\].*?)<br/>#i' => '<span class="log-error">$1</span><br/>',
				'#(\[SUMMARY\].*?)<br/>#i' => '<span class="log-summary">$1</span><br/>',
];

?>

<div id="admin">
				<pre style="font-family: monospace">
								<?php 
								
								$a = str_replace("\n", '<br/>', $log);
								
								foreach($patterns as $pattern => $replace)
								{
												$a = preg_replace($pattern, $replace, $a);
								}
								
								echo $a;
												
								?>
				</pre>
</div>

<style type="text/css">
.log-important
{
				color: #ddaa00;
}
.log-summary
{
				color: #080;
}
.log-warning
{
				color: #FF8A00;
}
.log-error
{
				color: #f00;
}
</style>