<?php

use yii\helpers\Url;
use app\models\Product;
use app\helpers\AlertHelper;
use app\helpers\CurrencyHelper;
use app\models\Subscription;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

?>

<div id="admin">
				
				<?= AlertHelper::get() ?>
				
				<?php GridView::begin([
								'id' => 'admin-list-products',
								'dataProvider' => new ActiveDataProvider([
												'query' => Product::find(),
												'pagination' => [
																'pageSize' => 50,
												],
								]),
								'columns' => [
												['class' => yii\grid\SerialColumn::className()],
												'name' => [
																'format' => 'raw',
																'label' => 'Name',
															 'value' => function($data)
																{
																				$name = $data->name;
																				
																				if (strlen($name) > 35)
																				{
																								$name = substr($name, 0, 35).'...';
																				}
																				
																				$name = '<span title="'.$data->name.'">'.$name.'<span style="hidden"><span>';
																				
																				return $name;
																},
																'attribute' => 'name',
																'contentOptions' => ['class' => 'nowrap'],
												],
												'status' => [
																'format' => 'raw',
																'label' => 'Status',
																'attribute' => 'status',
																'value' => function($data){
																				return '<span>'.$data->getStatusBadge().'</span>';
																},
																'contentOptions' => ['class' => 'nowrap text-center'],
												],
												'url' => [
																'format' => 'raw',
																'label' => 'Website',
															 'value' => function($data)
																{
																				$url = $data->url;
																				
																				if (strlen($url) > 30)
																				{
																								$url = substr($url, 0, 30).'...';
																				}
																				
																				$url = '<a data-url="'.$data->url.'" href="'.$data->url.'" title="'.$data->url.'">'.$url.'</a> <span class="url-info pull-right"></span>';
																				
																				return $url;
																},
																'attribute' => 'url',
																'contentOptions' => ['class' => 'nowrap url'],
												],
												'priceElement' => [
																'format' => 'raw',
																'label' => 'CSS path',
															 'value' => function($data)
																{
																				$path = $data->priceElement;
																				
																				if (strlen($path) > 30)
																				{
																								$path = '...'.substr($path, strlen($path) - 30);
																				}
																				
																				$path = '<span title="'.$data->priceElement.'">'.$path.'</span>';
																				
																				return $path;
																},
																'contentOptions' => ['class' => 'nowrap'],
												],
												'author' => [
																'attribute' => 'author.username',
																'label' => 'Author',
																'format' => 'raw',
																'value' => function ($data){
																				return isset($data->author) ? '<span data-id="'.$data->author['id'].'">'.$data->author['username'].'</span>' : null;
																},
																'contentOptions' => ['class' => 'user'],
												],
												'timeAdded' => [
																'format' => 'raw',
																'label' => 'Added',
																'value' => function($data){
																				$date = Yii::$app->formatter->asDate($data->timeAdded, 'php:d F Y');
																				$time = Yii::$app->formatter->asTime($data->timeAdded);
																				return '<span class="datetimeToggle" data-shown="date" data-time="'.$time.'" data-date="'.$date.'">'.$date.'</span>';
																},
																'attribute' => 'timeAdded',
																'contentOptions' => ['class' => 'nowrap'],
												],
												'price' => [
																'format' => 'raw',
																'label' => 'Price',
																'value' => function($data){
																				return '<span data-id="'.$data->id.'" class="price"><i class="fa fa-gear fa-spin"></i></span>';
																},
																'contentOptions' => ['class' => 'nowrap'],
												],
												'action' => [
																'format' => 'raw',
																'value' => function($data){
																				$buttons = '';
																				$subscribed = false;
																				
																				if (Subscription::findOne(['productId' => $data->id]) !== null)
																				{
																								$subscribed = true;
																				}																	
																				
																				$buttons .= '<a href="'.Url::to(['product/update', 'id' => $data->id]).'" style="color: #444"><i class="fa fa-pencil fa-fw"></i></a>';
																				$buttons .= ' <a class="remove" onclick="'.($subscribed ? 'return stopLink();' : '').'" href="'.Url::to(['product/delete', 'id' => $data->id]).'" style="color: #944"><i class="fa fa-trash fa-fw"></i></a>';
																				$buttons .= $data->status == Product::STATUS_NEW ? ' <a href="'.Url::to(['product/approve', 'id' => $data->id]).'" style="color: #494"><i class="fa fa-check"></i></a>' : null;
															
																				return $buttons;
																},
																'contentOptions' => ['class' => 'nowrap text-center'],
												],
												'subscription' => [
																'format' => 'raw',
																'value' => function($data){
																				$subscribed = false;
																				
																				if (Subscription::findOne(['productId' => $data->id]) !== null)
																				{
																								$subscribed = true;
																				}
																				
																				return $subscribed ? '<i class="fa fa-envelope" style="color: #444"></i>' : '';
																}
												],
								],
				]); ?>
				
				<?php GridView::end(); ?>
</div>

<script type="text/javascript">
				
function stopLink()
{
				return confirm('Are you sure you want to delete this product? Users are subscribed to it.');
}
				
$(document).ready(function(){
				$('#admin-list-products table tbody tr td.user').click(function(){
								var uid = $(this).find('span').data('id');
								console.log(uid);
								if (uid !== undefined)
								{
												$('.sidebar-container .user-info .header .working').show();
												
												$.ajax({
																url: '<?= Url::to(['user/ajax-details']) ?>?id='+uid,
																method: 'get'
												}).done(function(data){
																var user = JSON.parse(data);

																$('.sidebar-container .user-info .header .working').hide();
																
																$('.sidebar-container .user-info .content').html(
																				'<table>'
																								+'<tr><td class="b">Id</td><td>' + user.id + '</td></tr>'
																								+'<tr><td class="b">Username</td><td>' + user.username + '</td></tr>'
																								+'<tr><td class="b">Email</td><td>' + user.email + '</td></tr>'
																								+'<tr><td class="b">Created</td><td>' + user.timeCreated + '</td></tr>'
																								+'<tr><td class="b">Status</td><td>' + user.status + '</td></tr>'
																								+'<tr><td class="b">Role</td><td>' + user.role + '</td></tr>'
																								+'<tr><td class="b">Products added</td><td><span class="b" style="color: #080">' + user.productsApproved + '</span>' + (user.productsUnapproved !== undefined ? (' | <span class="b" style="color: #b80">' + user.productsUnapproved + '</span>') : '') + '</td></tr>'
																				+'</table>'
																);
												});			
								}
								else
								{
												$('.sidebar-container .user-info .content').html('<i>No user selected</i>');
												$('.sidebar-container .user-info .header .working').hide();
								}
				});
				
				$('#admin-list-products table tbody tr').each(function(){
								var id = $(this).find('span.price').data('id');
								
								$.ajax({
												url: '<?= Url::to(['product/ajax-price']) ?>?id='+id,
											 method: 'get',
								}).done(function(data){
												update(id, data, 'price');
								}).fail(function(){
												update(id, false, 'price');
								});
				});
			
				$('#admin-list-products table tbody tr td.url').click(function(){				
				
								var id = $(this).parent('tr').find('span.price').data('id');
								var urlToCheck = $(this).find('a').data('url');
								
								$(this).find('.url-info').html('<i class="fa fa-gear fa-spin"></i>');
								
								$.ajax({
												url: '<?= Url::to(['product/ajax-check-url']) ?>?url='+urlToCheck,
											 method: 'get',
								}).done(function(data){
												update(id, data, 'url');
								});
				});
				
				$('.datetimeToggle').parent('td').click(function(){toggle($(this).find('.datetimeToggle'))});
				$('.datetimeToggle').parent('td').hover(function(){toggle($(this).find('.datetimeToggle'));}, function(){toggle($(this).find('.datetimeToggle'));});
});		

function toggle(obj, hard)
{
				if (obj.data('shown') === 'date')
				{
								obj.data('shown', 'time');
								obj.text(obj.data('time'));
				}
				else
				{
								obj.data('shown', 'date');
								obj.text(obj.data('date'));
				}
}

function update(id, data, type)
{
				if (type === 'price')
				{
								if (data !== false)
								{
												$('#admin-list-products table tbody tr[data-key='+id+'] .price').text(data);
								}
								else
								{
												$('#admin-list-products table tbody tr[data-key='+id+'] .price').html('<span class="label label-danger">Error</span>');
								}
				}
				if (type === 'url')
				{
								var obj = $('#admin-list-products table tbody tr[data-key='+id+'] .url .url-info');
								if (data.indexOf('200') > -1)
								{
												obj.html('<i class="fa fa-check" style="color: #080;"></i>');
								}
								else
								{
												obj.html('<span class="label label-danger">'+data+'</span>');
								}
				}
}
</script>