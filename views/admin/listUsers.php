<?php

use yii\helpers\Url;
use app\models\User;
use app\helpers\AlertHelper;
use app\helpers\CurrencyHelper;
use app\models\Subscription;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

?>

<div id="admin">
				
				<?= AlertHelper::get() ?>
				
				<?php GridView::begin([
								'id' => 'admin-list-users',
								'dataProvider' => new ActiveDataProvider([
												'query' => User::find(),
												'pagination' => [
																'pageSize' => 50,
												],
								]),
								'columns' => [
												['class' => yii\grid\SerialColumn::className()],
												'username:text:Username',
												'email:text:Email',
												'timeCreated:datetime:Created',
												'status' => [
																'format' => 'html',
																'value' => function($data){
																				return $data->statusBadge;
																},
																'label' => 'Status',
												],
												'role' => [
																'format' => 'html',
																'value' => function($data){
																				return $data->roleBadge;
																},
																'label' => 'Role',
												],
												'action' => [
																'format' => 'raw',
																'value' => function($data){
																				$buttons = '';
																				$subscribed = false;
																				
																				if (Subscription::findOne(['productId' => $data->id]) !== null)
																				{
																								$subscribed = true;
																				}																	
																				
																				return $data->status != User::STATUS_SUSPENDED 
																								?	'<a href="'.Url::to(['user/ban', 'id' => $data->id]).'"><i class="fa fa-ban text-danger"></i></a>' 
																								:	'<a href="'.Url::to(['user/unban', 'id' => $data->id]).'"><i class="fa fa-ban text-info"></i></a>';
																},
																'contentOptions' => ['class' => 'nowrap text-center'],
												],
								],
				]); ?>
				
				<?php GridView::end(); ?>
</div>
