<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\AlertHelper;
use app\helpers\CurrencyHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;

$currencies = [];

foreach (CurrencyHelper::AVAILABLE_CURRENCIES as $iso => $currency)
{
				$c = $iso.' - '.$currency;
				$currencies[$iso] = $c;
}

?>
<?= AlertHelper::get() ?>

<div class="user-update clearfix">
<div class="col-md-6">
				<?php $form = ActiveForm::begin([
								'id' => 'update-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-md-7\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-md-2 control-label'],
        ],
				]); ?>

				<div class="col-lg-12">
				
								<h1>Settings</h1>
								
								<?= $form->field($model, 'username')->textInput(['value' => $model->username]) ?>
				    <?= $form->field($model, 'email')->textInput(['value' => $model->email]) ?>
       	<?= $form->field($model, 'currency')->dropDownList($currencies) ?>
				</div>
				
				<div class="col-md-4 col-md-offset-2">
								<?= Html::submitButton('Update', ['class' => 'btn btn-primary col-sm-12', 'name' => 'submit-button']) ?>
				</div>
				<?php ActiveForm::end(); ?>
</div>
<div class="col-md-6">
				<?php $form = ActiveForm::begin([
								'id' => 'password-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-md-7\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-md-5 control-label'],
        ],
				]); ?>

				<div class="col-lg-12">
								<h1 class="col-lg-offset-1">Change password</h1>
				
								<?= $form->field($modelPassword, 'password')->passwordInput(); ?>
				    <?= $form->field($modelPassword, 'newPassword')->passwordInput(); ?>
				    <?= $form->field($modelPassword, 'newPasswordRepeat')->passwordInput(); ?>
				</div>
				
				<div class="col-md-4 col-md-offset-5  user-update-password">
								<?= Html::submitButton('Change password', ['class' => 'btn btn-danger col-sm-12', 'name' => 'submit-button']) ?>
				</div>
				<?php ActiveForm::end(); ?>
</div>
</div>