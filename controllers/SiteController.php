<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use app\models\forms\ContactForm;
use app\models\forms\SignupForm;
use app\models\User;
use yii\helpers\Url;
use app\helpers\DateTimeHelper;
use yii\base\DynamicModel;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
				
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
				
    public function actionHelp()
    {
        return $this->render('help');
    }
				
				public function actionActivate($code)
				{
								$user = User::find()->where(['activationCode' => $code, 'status' => User::STATUS_INACTIVE])->one();
								
								if ($user !== null)
								{
								
												$user->status = User::STATUS_ACTIVE;

												if ($user->save())
												{
																Yii::$app->session->setFlash('success', 'Your account has been succesfully activated. You can now log in.');
												}
								}
								else
								{
												Yii::$app->session->setFlash('danger', "Couldn't find the user in the database. The URL is damaged or the account has already been activated.");
								}
								
								$form = new LoginForm;
								
								return $this->redirect(Url::to(['site/login']));
				}

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
								
        if ($model->load(Yii::$app->request->post())) 
								{
												if ($model->login())
												{
																return $this->goBack();
												}
												else
												{
																return $this->render('login', 
																[
																				'model' => $model,
																]);
												}
        } 
								else 
								{
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
				
				public function actionSignup()
				{
								if(!\Yii::$app->user->isGuest) {
												return $this->goHome();
								}
								
								$form = new SignupForm;
								
								if ($form->load(Yii::$app->request->post()) && $form->register())
								{
												return $this->render('signup', [
																'model' => $form,
																'success' => true,
												]);
								}
								
								return $this->render('signup', [
            'model' => $form,
												'success' => false,
        ]);
				}

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm(['scenario' => Yii::$app->user->isGuest ? 'asGuest' : 'asUser']);
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['contactEmail'])) 
								{
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } 
								else 
							 {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
				
				public function actionResendEmail()
				{
								if (Yii::$app->user->isGuest)
								{
												$model = Yii::$app->request->post();
												
												if (!$model)
												{
																$model = new DynamicModel(['email_username' => '']);
												
																return $this->render('resendEmail', ['model' => $model]);
												}

												$user = User::find()->where('email = :email_username OR username = :email_username', [':email_username' => $model['DynamicModel']['email_username']])->one();
												
												if ($user === null)
												{
																$model = new DynamicModel(['email_username' => '']);
																
																Yii::$app->session->setFlash('danger', "User with such username or email does not exist.");
																
																return $this->render('resendEmail', ['model' => $model]);
												}
												
												$user->sendActivationEmail(true);
												
												Yii::$app->session->setFlash('success', "Your activation email was succesfully resent.");
												
												$model = new DynamicModel(['email_username' => '']);
												
												return $this->render('resendEmail', ['model' => $model]);
								}
								
								return $this->goBack();
				}
}
