<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Company;
use Yii;

class CompanyController extends Controller
{
				public function beforeAction($action)
				{
								if (!Yii::$app->user->isGuest)
								{
												return parent::beforeAction($action);
								}
								return false;
				}
				
				public function actionCreate($name)
				{
								$company = Company::findOne(['name' => $name]);
								
								if ($company === null)
								{
												$company = new Company;
												$company->name = $name;
												$company->status = Company::STATUS_NEW;
												
												if ($company->save())
												{
																Yii::$app->session->setFlash('success', 'The company was succesfully added. It now has to be approved by the admin.');
												}
												else
												{
																Yii::$app->session->setFlash('danger', 'There was an error while adding the company.');
												}
								}
								else
								{
												Yii::$app->session->setFlash('danger', 'This company already exists.');
								}
								
								return $this->goBack();
				}
}