<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\User;
use yii\helpers\Url;
use app\models\Product;
use app\models\Company;
use Yii;

class AdminController extends Controller
{
				public function beforeAction($action)
				{
								if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN)
								{
												return parent::beforeAction($action);
								}
								
								return $this->redirect(Url::to(['site/login']));
				}
				
				public function actionListUsers()
				{
								$users = User::find()->all();
								
								return $this->render('listUsers', ['users' => $users]);
				}
				
				public function actionListProducts()
				{
								$products = Product::find()->all();
								
								return $this->render('listProducts', ['products' => $products]);
				}
				
				public function actionDeleteUser($id)
				{
								$user = User::findOne(['id' => $id]);
								
								return $this->redirect(Url::to(['admin/list-users']));
				}
				
				public function actionListCompanies()
				{
								$companies = Company::find()->all();
								
								return $this->render('listCompanies', ['companies' => $companies]);
				}
				
				public function actionConsoleLog()
				{
								$log = file_get_contents(Url::to('@runtime/logs/console.log'));
								
								return $this->render('consoleLog', ['log' => $log]);
				}
}