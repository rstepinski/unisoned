<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Subscription;
use app\models\User;
use app\models\forms\SettingsForm;
use app\models\forms\PasswordChangeForm;
use app\models\Product;

class UserController extends Controller
{
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
				
				public function actionUpdate()
				{
							 if (!Yii::$app->user->isGuest)
								{
												$model = new SettingsForm;
												$modelPassword = new PasswordChangeForm;
												$user = Yii::$app->user->identity;
												
												if($model->load(Yii::$app->request->post()) && $model->update())
												{
																Yii::$app->session->setFlash('success', 'Your account was succesfully updated.');
												}
												
												if($modelPassword->load(Yii::$app->request->post()) && $modelPassword->updatePassword())
												{
																Yii::$app->session->setFlash('success', 'Your password was sucessfully changed.');
												}
												
												if(!$model->hasErrors())
												{
																$model->username = $user->username;
																$model->email = $user->email;
																$model->currency = $user->currency;
												}
												
												return $this->render('update', ['model' => $model, 'modelPassword' => $modelPassword]);
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
				}
				
				public function actionAjaxDetails($id)
				{
								$user = User::findOne(['id' => $id]);
								
								$return = new \stdClass;
								
								$return->id = $user->id;
								$return->username = $user->username;
								$return->email = $user->email;
								$return->timeCreated = $user->timeCreated;
								$return->status = $user->statusBadge;
								$return->role = $user->roleBadge;
								$return->currency = $user->currency;
								$return->productsApproved = Yii::$app->db->createCommand('SELECT COUNT(*) FROM product WHERE status = :status AND authorId = :uid', [':status' => Product::STATUS_APPROVED, ':uid' => $user->id])->queryAll()[0]['COUNT(*)'];
								$return->productsUnapproved = Yii::$app->db->createCommand('SELECT COUNT(*) FROM product WHERE status = :status AND authorId = :uid', [':status' => Product::STATUS_NEW, ':uid' => $user->id])->queryAll()[0]['COUNT(*)'];
								
								return json_encode($return);
				}
}
