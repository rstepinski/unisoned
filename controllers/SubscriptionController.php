<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Subscription;
use app\models\Product;
use yii\helpers\Url;
use app\models\Company;

class SubscriptionController extends Controller
{
				public function beforeAction($action)	
				{
								Yii::$app->user->returnUrl = Yii::$app->request->referrer;
								
								return parent::beforeAction($action);
				}
				
				public function actionAdd($product)
				{
								if (!Yii::$app->user->isGuest)
								{
												Subscription::create($product);
												
												$this->goBack();
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
				}
				
				public function actionAddByCompany($company)
				{
								if (!Yii::$app->user->isGuest)
								{
												$products = Product::findAll(['companyId' => $company, 'status' => Product::STATUS_APPROVED]);
												$user = Yii::$app->user->identity;

												$success = 0;
												$total = 0;

												foreach ($products as $product)
												{
																$total++;
																$success += Subscription::create($product, false) ? 1 : 0;
												}

												if ($success > 0 && $total > 0) { Yii::$app->session->setFlash('success', "$success out of $total products were added to your subscription list."); }
												else if ($success == 0 && $total > 0) { Yii::$app->session->setFlash('warning', "$success out of $total products were added to your subscription list. Probably you already subscribed to them."); }
												else if (total == 0) { Yii::$app->session->setFlash('danger', "This company has no products."); }

												$this->goBack();
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
				}
				
				public function actionRemove($product)
				{
								if (!Yii::$app->user->isGuest)
								{
												$user = Yii::$app->user->identity;
												
												$subscription = Subscription::findOne(['productId' => $product, 'userId' => Yii::$app->user->identity->id]);
												$product = Product::findOne(['id' => $product]);
												
												if ($subscription === null)
												{
																Yii::$app->session->setFlash('warning', "You haven't subscribed to that product.");
												}
												else
												{
																if ($subscription->delete() !== false)
																{
																				Yii::$app->session->setFlash('success', "Your subscription for product $product->name was succesfully removed.");
																}
																else
																{
																				Yii::$app->session->setFlash('danger', "There was an error while processing your request. Please try again in a few minutes. If the problem persists, please contact us.");
																}
												}
												
												$this->goBack();
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
				}
				
				public function actionList($page = 1)
				{
								if (!Yii::$app->user->isGuest)
								{
												$user = Yii::$app->user->identity;
												
												$pages = ceil(Yii::$app->db->createCommand('SELECT COUNT(*) FROM subscription WHERE userId = '.$user->id)->queryAll()[0]['COUNT(*)'] / Subscription::ITEMS_PER_PAGE);
								
											 $model = Subscription::getByUser(Yii::$app->user->identity->id, $page);
												
												if ($page > $pages) { $page = $pages; }
												
												if (empty($model))
												{
																Yii::$app->session->setFlash('info', 'You have no subscriptions yet.');
												}
												
												$companies = Company::find()->orderBy('name')->all();
												
												return $this->render('list', ['subscriptions' => $model, 'companies' => $companies, 'pages' => $pages, 'page' => $page]);
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
				}
}
