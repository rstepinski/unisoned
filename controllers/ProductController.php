<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Product;
use app\helpers\DateTimeHelper;
use app\helpers\CurrencyHelper;
use yii\data\Sort;
use yii\data\ActiveDataProvider;
use app\models\Company;
use app\models\Category;
use yii\helpers\Url;
use app\models\Rating;
use app\models\User;
use app\models\Subscription;
use app\models\forms\ProductForm;
use Yii;

class ProductController extends Controller
{
				public function beforeAction($action)
				{
								Yii::$app->user->returnUrl = Yii::$app->request->referrer;
								
								return parent::beforeAction($action);
				}
				
				public function actionUpdate($id)
				{
								$product = Product::findOne(['id' => $id]);
								
								if ($product !== null)
								{
												$form = new ProductForm;
												
												if ($form->load(Yii::$app->request->post()) && $form->update($product->id))
												{
																Yii::$app->session->setFlash('success', 'The product was updated succesfully.');
																
		              $product = Product::findOne(['id' => $id]);
												}
												
												return $this->render('update', ['model' => $form, 'product' => $product]);
								}
								
								Yii::$app->session->setFlash('danger', 'This product does not exist.');
								return $this->goBack();
				}
				
    public function actionList($sort = null, $onlyApproved = 1, $search = null)
    {
								$sort = new Sort([
												'attributes' => [
																'name',
																'company.name',
																'category.name',
																'unifiedPrice' => [
																				'asc' => ['priceCacheUSD' => SORT_ASC],
																				'desc' => ['priceCacheUSD' => SORT_DESC],
																				'default' => SORT_ASC,
																],
												],
								]);
								
								$query = Product::find()->joinWith(['company', 'category']);
								$query = $onlyApproved ? $query->where(['product.status' => Product::STATUS_APPROVED]) : $query;
								$query = $search !== null ? $query->where('product.name LIKE :name', [':name' => '%'.$search.'%']) : $query;
								
								$query = $query->orderBy($sort->orders);
								
							 $provider = new ActiveDataProvider([
												'query' => $query,
												'pagination' => [
																'pageSize' => $onlyApproved ? 25 : 10,
												],
												'sort' => $sort,
								]);
								
        return $this->render('//product/list', [
												'sort' => ['attributes' => ['name', 'company.name', 'category.name', 'unifiedPrice']],
												'provider' => $provider,
												'onlyApproved' => $onlyApproved,
												'search' => $search,
								]);
    }
				
				public function actionRate($rating, $product)
				{
								if (!Yii::$app->user->isGuest)
								{
												if (is_numeric($rating))
												{
																$uid = Yii::$app->user->identity->id;

																$p = Product::findOne(['id' => $product]);
																if ($p)
																{
																				$r = Rating::findOne(['userId' => $uid, 'productId' => $product]);
																				if (empty($r))
																				{
																								$r = new Rating;
																				}

																				$r->rating = $rating;
																				$r->userId = $uid;
																				$r->productId = $product;

																				$r->save();
																}
																else
																{
																				Yii::$app->session->setFlash('danger', 'The product does not exist');
																}
												}
												else
												{
																Yii::$app->session->setFlash('danger', 'The rating must be a numeric value');
												}
												
												return $this->redirect(Url::to(['product/list']));
								}
								else
								{
												return $this->redirect(Url::to(['site/signin']));
								}
				}
    
    public function actionCreate()
    {
								Yii::$app->user->returnUrl = Url::to(['product/create']);
								
        if (!Yii::$app->user->isGuest)
								{
												$form = new Product;
												
												if (!empty(Yii::$app->request->post()['Product']))
												{
																$good = 0;
																$total = 0;
																
																$items = Yii::$app->request->post()['Product'];
																
																foreach ($items as $item)
																{
																				if ($item['name'] && $item['url'] && $item['priceElement'])
																				{
																								if (Product::create($item)) { $good++; }
																								$total++;
																				}
																}
																
																if ($good == $total && $total > 0) { Yii::$app->session->setFlash('success', "$good of $total products were added to the database."); }
																if ($good != $total && $good > 0 && $total > 0) { Yii::$app->session->setFlash('warning', "$good of $total products were added to the database."); }
																if ($good == 0 && $total > 0) { Yii::$app->session->setFlash('danger', "$good of $total products were added to the database."); }
																if ($total == 0) { Yii::$app->session->setFlash('warning', "You didn't input any data!"); }
																
												}
												
												$categories = Category::find()->select('name')->all();
												$companies = Company::find()->orderBy('status DESC, name ASC')->all();
												$categoriesArr = [];
												$companiesArr = [];
												
												foreach ($categories as $category)
												{
																$categoriesArr[] = $category->name;
												}
												foreach ($companies as $company)
												{
																$companiesArr[] = $company->status == Company::STATUS_NEW ? $company->name.' (!)' : $company->name;
												}
								
												$categoriesJSON = json_encode($categoriesArr);
												$companiesJSON = json_encode($companiesArr);
												
												return $this->render('create', [
																'model' => $form,
																'categories' => $categoriesJSON,
																'companies' => $companiesJSON,
												]);
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
    }
				
				public function actionApprove($id)
				{
								if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN)
								{												
												$product = (new Product)->getById($id);
												if ($product !== null)
												{
																$product->status = Product::STATUS_APPROVED;
																$product->save();
												}
												
												return $this->goBack();
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
				}
    
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN)
								{												
												$product = (new Product)->getById($id);
												if ($product !== null) 
												{ 
																Subscription::deleteAll(['productId' => $id]);
																$product->delete(); 
												}
												
												return $this->goBack();
								}
								else
								{
												return $this->redirect(Url::to(['site/login']));
								}
    }
				
				public function actionHelp()
				{
								return $this->render('help');
				}
				
				public function actionAjaxConvert($value, $from, $to)
				{
								return Yii::$app->formatter->asCurrency(CurrencyHelper::convert($value, $from, $to), CurrencyHelper::getSymbol($to));
				}
				
				public function actionAjaxPrice($id)
				{
							 $p = Product::findOne(['id' => $id]);
								
								if (is_numeric($p->price) && $p->price != 0)
								{
												return Yii::$app->formatter->asCurrency($p->price, CurrencyHelper::getSymbol($p->priceCurrency));
								}	
								throw new Exception('Price retrieve error');
				}
				
				public function actionAjaxCheckUrl($url)
				{						
								if (filter_var($url, FILTER_VALIDATE_URL))
								{
												stream_context_set_default(['http' => ['method' => 'HEAD']]);
												
												$headers = @get_headers($url);
												
												if($headers)
												{
																return $headers[0];
												}
												return '404';
								}
								
								return 'Not an URL';
				}
}
