<?php 

namespace app\views\mail;

use yii\helpers\Url;
use Yii;

?>

<img src="<?= Url::toRoute('@web/img/unisoned_b.png', true) ?>" height="70"/><br/>

<h2>Greetings, <?= $user->username ?>!</h2>

<p>There've been some changes in product prices.</p><br/>

<table style="width: 100%">
				<tr style="background-color: #000; color: #fff"><th>Name</th><th>Old price</th><th>New price</th>
											
				<?php foreach ($products as $id => $product): ?>
								
				<tr style="width: 100%; text-align: center; <?= $id%2==0 ? '' : 'background-color: #e3e3e3;' ?>">
								<td><?= $product['product']->name ?></td>
								<td><?= Yii::$app->formatter->asCurrency($product['oldPrice'], $converter->getSymbol($product['product']->priceCurrency)) ?>
												<?php if ($product['product']->priceCurrency != $user->currency): ?> (<?= Yii::$app->formatter->asCurrency($converter->convert($product['oldPrice'], $product['product']->priceCurrency, $user->currency, 3600), $converter->getSymbol($user->currency)) ?>) <?php endif; ?></td>
								<td><?= Yii::$app->formatter->asCurrency($product['product']->price, $converter->getSymbol($product['product']->priceCurrency)) ?>
												<?php if ($product['product']->priceCurrency != $user->currency): ?> (<?= Yii::$app->formatter->asCurrency($converter->convert($product['product']->price, $product['product']->priceCurrency, $user->currency, 3600), $converter->getSymbol($user->currency)) ?>) <?php endif; ?></td>
				</tr>
				<?php endforeach; ?>
</table>