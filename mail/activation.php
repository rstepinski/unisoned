<?php 

namespace app\views\mail;

use yii\helpers\Url;

$url = Url::to(['site/activate', 'code' => $user->activationCode], true);

?>
<img src="<?= Url::to('@web/img/unisoned_b.png', true) ?>" height="70"/><br/>

<h2>Greetings, <?= $user->username ?>!</h2>

<?php if($resend): ?>
				<p>You've requested another activation email.</p>
<?php else: ?>
				<p>Thank you for your registration at UNISONED. We hope you'll find our website useful.</p>
<?php endif; ?>
<p>To activate your account, please click this link:</p>
<a href="<?= $url ?>"><?= $url ?></a>