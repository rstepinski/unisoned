$('document').ready(function(){
    $('._site._about').find('.content-wrapper').addClass('col-md-9 col-sm-12');
    $('._site._login').find('.content-wrapper').addClass('col-md-7 col-sm-12');
    $('._site._contact').find('.content-wrapper').addClass('col-md-8 col-sm-12');
    $('._subscription._list').find('.content-wrapper').addClass('col-md-10 col-sm-12');
    
    a(['.content'], 0);
});

function a(css, i)
{  
    $(css[i]).animate({
        opacity: 'show',
    });
    
    if (i < css.length)
    {
        i++;
        setTimeout(function(){a(css, i);}, 50);
    }
}