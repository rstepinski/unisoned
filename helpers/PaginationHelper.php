<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

class PaginationHelper
{
				const BUTTONS_LIMIT = 3;
				private	static $params = [];
			 private	static $route = [];
				
				static function getButtons($pages, $page, $maxButtons = self::BUTTONS_LIMIT, $route = null)
				{
								$buttons = '';
								$start;
								$finish;
								
								if ($maxButtons % 2 == 0)
								{
												$maxButtons++;
								}
								
								if ($route === null)
								{
												self::$route[] = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
												self::$params = Yii::$app->controller->actionParams;
								}
								
								if (($maxButtons - 1) / 2 >= $page)
								{
												$start = 1;
								}
								else if ($pages - $page < ($maxButtons - 1) / 2)
								{
												$start = $pages - ($maxButtons - 1);
												$start = $start < 1 ? 1 : $start;
								}
								else
								{
												$start = $page - (($maxButtons - 1) / 2);
								}
								
								if ($start + $maxButtons > $pages)
								{
												$finish = $pages + 1;
								}
								else
								{
												$finish = $start + $maxButtons;
								}
								
								if ($page != 1)
								{
												$buttons .= '<a class="btn btn-small btn-active" href="'.
																				Url::to(self::routePage(1)).'"><i class="glyphicon glyphicon-fast-backward"></i></a>';
												
												$buttons .= '<a class="btn btn-small btn-active" href="'.
																				Url::to(self::routePage($page - 1)).'"><i class="glyphicon glyphicon-triangle-left"></i></a>';
								}
								
								for ($i = $start; $i < $finish; $i++)
								{
												$buttons .= '<a class="btn btn-small '.($i == $page ? 'btn-info' : 'btn-active').'" href="'.
																				Url::to(self::routePage($i)).'">'.$i.'</a>';
								}
								
								if ($page != $pages)
								{
												$buttons .= '<a class="btn btn-small btn-active" href="'.Url::to(self::routePage($page + 1)).'"><i class="glyphicon glyphicon-triangle-right"></i></a>';
												
												$buttons .= '<a class="btn btn-small btn-active" href="'.Url::to(self::routePage($pages)).'"><i class="glyphicon glyphicon-fast-forward"></i></a>';
								}
								
								return $buttons;
				}
				
				private function routePage($page)
				{
								self::$params['page'] = $page;
								self::$route = array_merge(self::$route, self::$params);
								return self::$route;
				}
}

