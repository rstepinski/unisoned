<?php

namespace app\helpers;

use Yii;

class	AlertHelper	
{
				public static function get()
				{
								$alerts = '';
								
								foreach (Yii::$app->session->getAllFlashes(true) as $key => $message)
								{
												$icon = ''; 
												
												switch ($key)
												{
																case 'danger': $icon = '<i class="glyphicon glyphicon-remove-sign"></i>'; break;
																case 'warning': $icon = '<i class="glyphicon glyphicon-exclamation-sign"></i>'; break;
																case 'info': $icon = '<i class="glyphicon glyphicon-info-sign"></i>'; break;
																case 'success': $icon = '<i class="glyphicon glyphicon-ok-sign"></i>'; break;
												}
												
												if (is_array($message))
												{
																foreach ($message as $m)
																{
																				$alerts .= '<div class="alert alert-'.$key.'">'.$icon.'<div>'.$m.'</div></div>';
																}
												}
												else
												{
																$alerts .= '<div class="alert alert-'.$key.'">'.$icon.'<div>'.$message.'</div></div>';
												}
								}
								
								return $alerts;
				}
}
