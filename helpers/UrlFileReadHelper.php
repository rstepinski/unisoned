<?php

namespace app\helpers;

class UrlFileReadHelper 
{
    static function readFile($url)
    {
        $curl = curl_init();
        $userAgent = 'Firefox/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

        curl_setopt($curl, CURLOPT_URL, $url); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5); 

        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); 
        curl_setopt($curl, CURLOPT_TIMEOUT, 5); 	

        $contents = curl_exec($curl);

        if ($contents=="")
        {
            $headers = @get_headers($url)[0];
            if ($headers && strpos($headers, '404') === false)
            {
                $contents = file_get_contents($url);
            }
            else
            {
                return 'ERR_404';
            }
        }

        curl_close($curl);
								
        return $contents;
    }
}