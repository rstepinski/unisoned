<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;
use app\models\Rating;

class StarRatingHelper
{
				const STARS = 6;
				
				static function getStars($rating, $objectId, $canRate = true, $stars = self::STARS)
				{
								$rating = self::roundHalf($rating);
								
								$starsText = '';
								$tag = $canRate ? 'a' : 'span';
								
								for ($i = 1; $i <= $stars; $i++)
								{
												$url = $canRate ? Url::to(['product/rate', 'rating' => $i, 'product' => $objectId]) : '#';
												$classes = 'rate '.($canRate ? 'available' : '').' rating-'.$i;
												
												if ($rating >= $i)
												{
																$starsText .= '<'.$tag.' class="'.$classes.'" href="'.$url.'"><i class="fa fa-star"></i></'.$tag.'>';
																continue;
												}
												if ($rating >= $i - 0.5)
												{
																$starsText .= '<'.$tag.' class="'.$classes.'" href="'.$url.'"><i class="fa fa-star-half-o"></i></'.$tag.'>';
												}
												else
												{
																$starsText .= '<'.$tag.' class="'.$classes.'" href="'.$url.'"><i class="fa fa-star-o"></i></'.$tag.'>';
												}
								}
								
								return $starsText;
				}
				
				static function roundHalf($a)
				{
								$a = round($a, 2);
								
								$a *= 100;
								$rest = $a%100;
								
								if ($rest < 35) { return floor($a / 100); }
								if ($rest >= 35 && $rest < 65) { return floor($a / 100) + 0.5; }
								if ($rest >= 65) { return floor($a / 100) + 1; }
				}
}

