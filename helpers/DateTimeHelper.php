<?php

namespace app\helpers;

class DateTimeHelper
{	
    const DATE_FORMAT = 'Y-m-d';
    const DATETIME_FORMAT = 'Y-m-d H:i:s';
    const TIME_FORMAT = 'H:i:s';
    
    private static function format($datetime, $format = self::DATETIME_FORMAT)
    {
        return date($format, $datetime);
    }
    
    public static function now()
    {
	return self::format(time());
    }

    public static function add($amount, $range = 'days', $date = 'now')
    {
	return self::format(strtotime($date.' + '.$amount.' '.$range)); 
    }

    public static function subtract($amount, $range = 'days', $date = 'now')
    {
	return self::format(strtotime($date.' - '.$amount.' '.$range));
    }

    public static function compare($date1, $date2)
    {
	$result = null;

	$difference = strtotime($date2) - strtotime($date1);

	if ($difference > 0)
	{
	    $result = 1;
	}
	else
	{
	    if ($difference < 0)
	    {
		$result =  -1;
	    }
	    else
	    {
		$result = 0;
	    }
	}

	return $result;
    }

    public static function difference($date1 = 'now', $date2 = 'now', $range = 'days')
    {
	$result = null;

	$difference = strtotime($date2) - strtotime($date1);

	switch ($range)
	{
	    case 'seconds':
		$result = $difference;
		break;
	    case 'minutes':
		$result = $difference/60;
		break;
	    case 'hours':
		$result = $difference/3600;
		break;
	    case 'days':
		$result = $difference/86400;
		break;
	}

	return floor($result);
    }
}
