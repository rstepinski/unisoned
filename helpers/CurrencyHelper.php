<?php

namespace app\helpers;

use app\helpers\DateTimeHelper;
use yii\helpers\Url;

class CurrencyHelper 
{
    const AVAILABLE_CURRENCIES = [
        "AUD" => 'Australian dollar',
        "BGN" => 'Bulgarian lev',
        "BRL" => 'Brazilian real',
        "CAD" => 'Canadian dollar',
        "CHF" => 'Swiss franc',
        "CNY" => 'Chinese yuan',
        "CZK" => 'Czech koruna',
        "DKK" => 'Danish krone',
        "EUR" => 'Euro',
        "GBP" => 'British pound sterling',
        "HKD" => 'Hong Kong dollar',
        "HRK" => 'Croatian kuna',
        "HUF" => 'Hungarian forint',
        "IDR" => 'Indonesian rupiah',
        "ILS" => 'Israeli new shekel',
        "INR" => 'Indian rupee',
        "JPY" => 'Japanese yen',
        "KRW" => 'South Korean won',
        "MXN" => 'Mexican peso',
        "MYR" => 'Malaysian ringgit',
        "NOK" => 'Norwegian krone',
        "NZD" => 'New Zealand dollar',
        "PHP" => 'Phillippine peso',
        "PLN" => 'Polish złoty',
        "RON" => 'Romanian leu',
        "RUB" => 'Russian ruble',
        "SEK" => 'Swedish krona',
        "SGD" => 'Signapore dollar',
        "THB" => 'Thai baht',
        "TRY" => 'Turkish lyra',
        "USD" => 'US Dollar',
        "ZAR" => 'South african rand',
    ];

    static function convert($value, $from, $to)
    {
        $rate = self::getRate($from, $to);
								
        return round($value * $rate, 2);
    }

    static function getSymbol($iso)
    {
								$symbols = [
												'USD' => '$',
												'GBP' => '£',
												'EUR' => '€',
												'PLN' => 'zł',
												'JPY' => '¥',
												'AUD' => 'AU$',
												'CAD' => 'C$',
												'HKD' => 'HK$',
												'NZD' => 'NZ$',
												'KRW' => '₩',
												'ZAR' => 'R',
												'NOK' => 'kr',
												'SEK' => 'kr',
												'CNY' => '元',
												'CZK' => 'Kč',
												'HRK' => 'kn',
												'ILS' => '₪',
												'HUF' => 'Ft',
												'RON' => 'lei',
												'MYR' => 'RM',
												'THB' => '฿',
												'CHF' => 'Fr',
												'PHP' => '₱',
												'BRL' => 'R$',
												'BGN' => 'лв',
												'DKK' => 'kr',
												'IDR' => 'Rp',
								];

								if (isset($symbols[$iso]))
								{
												return $symbols[$iso];
								}

								return $iso;
    }
				
				public static function getRate($from, $to)
				{
								$path = Url::to('@app/runtime/rates/rates.cache');
								$handler = fopen($path, 'a+');
							 $content = filesize($path) > 0 ? fread($handler, filesize($path)) : "";
								$cacheRates = json_decode($content);
								fclose($handler);
								
								if (isset($cacheRates->$from->rates->$to) && DateTimeHelper::compare(DateTimeHelper::now(), $cacheRates->$from->validUntil) > 0)
								{
												return $cacheRates->$from->rates->$to;
								}
        else
        {
            $url = "http://api.fixer.io/latest?base=$from"; 
            $request = curl_init(); 
            $timeOut = 0; 
            curl_setopt ($request, CURLOPT_URL, $url); 
            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"); 
            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut); 
            $response = curl_exec($request); 
            curl_close($request);

            if (!isset(json_decode($response)->rates->$to))
            {
                return 'ERROR';
            }

            $rates = json_decode($response)->rates;
												
												if (!($cacheRates instanceof \stdClass)) { $cacheRates = new \stdClass; }
												if (!isset($cacheRates->$from) || ($cacheRates->$from instanceof \stdClass)) { $cacheRates->$from = new \stdClass; }
												
												$cacheRates->$from->validUntil = DateTimeHelper::add(2, 'hours');
												$cacheRates->$from->rates = $rates;
												
												$handler = fopen($path, 'w');
												fwrite($handler, json_encode($cacheRates));
												fclose($handler);
												
												return $rates->$to;
        }
				}
}