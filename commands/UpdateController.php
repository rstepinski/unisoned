<?php

namespace app\commands;

use yii\console\Controller;
use Yii;
use app\models\User;
use app\models\Product;
use app\helpers\DateTimeHelper;
use yii\helpers\Url;
use app\models\Subscription;
use app\helpers\CurrencyHelper;

class UpdateController extends Controller
{
				const EXECUTION_THRESHOLD = 20;
				const CHANGE_TABLE = 'changed_products';
				const UPDATE_TABLE = 'update_log';
				
				private $toMessage = []; // [user => [0 => [Product, oldPrice], 1 => [Product, oldPrice]], user2 => [0 => [Product, oldPrice]],...
				
    public function actionUpdate($begin = null)
    {
								$time_start = microtime(true);
								
								require(__DIR__ . '/../web/phpQuery/phpQuery.php');
								
								if ($begin === null)
								{
												if ($this->updateEmpty())
												{
																$this->dumpToTable();
												}
												
												$begin = $this->getNextId();
								}
								
        $products = Product::find()->where('id >= :id', [':id' => $begin])->all();
								
								$grandTotal = Yii::$app->db->createCommand('SELECT COUNT(*) FROM product')->queryAll()[0]['COUNT(*)'];
								$lastId = Product::find()->select('id')->orderBy('id DESC')->limit(1);
								
								$this->log("Starting database update from id = $begin.", 'info');
								
								$success = 0;
								$total = 0;
								
								$terminationId = $lastId;
								
								foreach ($products as $product)
								{
												$time_mid = microtime(true);
												$execution = ($time_mid - $time_start);
												
											 if ($execution > self::EXECUTION_THRESHOLD)
												{
																$this->log("Closing in on script termination time, dumping current data to database and starting over.", 'warning', 1);
																$this->log("Script execution time is $execution sec.", 'info', 1);
																$terminationId = $product->id;
																break;
												}
												
												$priceBefore = $product->priceCache;
												$price = $product->updatePrice();
												
												$total++;
												
												if ($price == 'error')
												{
																$this->log("Couldn't retrieve price for product '$product->name'. CSS Path or URL broken?", 'error', 1);
																continue;
												}
												
												if ($priceBefore != $price)
												{
																$this->log("Price for product '$product->name' has changed ($priceBefore -> $price)! Adding record to the database.", 'important', 1);
																$this->logUpdate($product, $priceBefore);
												}
												else
												{
																$this->log("Price for product '$product->name' updated, didn't change. Onto the next one.", 'info', 1);
												}
												
												$this->setUpdated($product->id);
												
												$success++;
								}
								
								$this->log("$success/$total out of $grandTotal database records successfully updated.", 'summary', 0);
								
								if ($lastId != $terminationId)
								{
												$this->log("Telling the updater to start over", 'info', 0);
												return 255;
								}
								
								$this->prepareEmails();
								
								$this->sendEmails();
								
								$this->truncateExistingRecords();
    }
				
				public function dumpToTable()
				{
								$products = Product::find()->all();
								
								foreach ($products as $product)
								{
												Yii::$app->db->createCommand()->insert(self::UPDATE_TABLE, [
																'productId' => $product->id,
																'updated' => 0,
												])->execute();
								}
				}
				
				public function setUpdated($id)
				{
								Yii::$app->db->createCommand()->update(self::UPDATE_TABLE, [
												'updated' => 1,
								], ['productId' => $id])->execute();
				}
				
				public function updateEmpty()
				{
								$result = Yii::$app->db->createCommand()->setSql('SELECT * FROM '.self::UPDATE_TABLE)->queryAll();
								
								return empty($result);
				}
				
				public function getNextId()
				{
								$result = Yii::$app->db->createCommand()->setSql('SELECT * FROM '.self::UPDATE_TABLE.' WHERE updated = 0 ORDER BY productId ASC')->queryOne();
								
								return $result ? $result['productId'] : false;
				}
				
				public function log($message, $type, $indentLevel = 0)
				{
								$file = fopen(trim(Url::toRoute('@runtime/logs/console.log'), '/'), 'a+');
								$indent = '';
								for ($i = 0; $i < $indentLevel; $i++) {$indent .= '    ';}
								$msg = DateTimeHelper::now().':'.$indent.'['.strtoupper($type).'] '.$message."\n";
								fwrite($file, $msg);
								echo $msg;
				}
				
				public function logUpdate($product, $oldPrice)
				{
								Yii::$app->db->createCommand()->insert(self::CHANGE_TABLE, [
												'productId' => $product->id,
												'oldPrice' => $oldPrice,
								])->execute();
								
								$this->log("Update product logged in the database.", 'info', 2);
				}
				
				public function prepareEmails()
				{								
								$products = Yii::$app->db->createCommand()->setSql('SELECT * FROM '.self::CHANGE_TABLE)->queryAll();
								
								foreach ($products as $product)
								{
												$this->log("Adding users to mail schedule", 'info', 1);

												$subscriptions = Subscription::findAll(['productId' => $product['productId']]);

												foreach ($subscriptions as $subscription)
												{
																$id = $subscription->productId;
																$this->toMessage[$subscription->userId][] = ['product' => $subscription->product, 'oldPrice' => $product['oldPrice']];
																$this->log("Added user $subscription->userId to mail schedule.", 'info', 2);
												}				
								}
				}
				
				public function sendEmails()
				{
								$this->log("Sending out emails...", 'info', 0);
								
								foreach ($this->toMessage as $userId => $products)
								{
												$user = User::findOne(['id' => $userId]);
												
												$converter = new CurrencyHelper;
												
												$this->log("Trying to mail user $userId ($user->username | $user->email)", 'info', 1);
												
												$sent = Yii::$app->mailer->compose('priceNotification', ['products' => $products, 'user' => $user, 'converter' => $converter])
																->setFrom(Yii::$app->params['adminEmail'])
																->setTo($user->email)
																->setSubject('UNISONED price notification')
																->send();
												
												if ($sent) { $this->log("Email sent successfully!", 'info', 2); }
												else { $this->log("Couldn't sent the email.", 'error', 2); }
								}
								
								$this->log("Emails sent.", 'summary', 0);
				}
				
				public function truncateExistingRecords()
				{
								$this->log("Cleaning update table...", 'info', 0);
								$result = Yii::$app->db->createCommand()->truncateTable(self::UPDATE_TABLE)->execute();
								$this->log("$result records removed.", 'summary', 0);
								
								$this->log("Cleaning change table...", 'info', 0);
								$result = Yii::$app->db->createCommand()->truncateTable(self::CHANGE_TABLE)->execute();
								$this->log("$result records removed.", 'summary', 0);
				}
}
