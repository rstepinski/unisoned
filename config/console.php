<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');
Yii::setAlias('@web', dirname(__DIR__) . '/web');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
				'defaultRoute' => 'update/update',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'showScriptName' => true,
            'enablePrettyUrl' => true,
												'scriptUrl' => '',
												'hostInfo' => 'http://',
        ],
        'db' => $db,
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
												'transport' => [
																'class' => 'Swift_SmtpTransport',
																'host' => 'poczta.interia.pl',
																'username' => 'twojastarajekupe@interia.pl',
																'password' => 'gorion123',
																'port' => '587',
																'encryption' => 'tls',
												],
        ],
    ],
    'params' => $params,
];
